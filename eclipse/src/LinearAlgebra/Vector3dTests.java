//William Trudel 1939241
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testVector() {
		Vector3d v=new Vector3d(2,3,4);
		assertEquals(2,v.getX());
		assertEquals(3,v.getY());
		assertEquals(4,v.getZ());
	}
	
	@Test
	void testMagnitude() {
		Vector3d v=new Vector3d(2,3,4);
		assertEquals(5.3851648,v.magnitude(),.000001);
	}
	
	@Test
	void testDotProduct() {
		Vector3d v1=new Vector3d(2,3,4);
		Vector3d v2=new Vector3d(5,6,7);
		assertEquals(56,v1.dotProduct(v2));
	}
	
	@Test
	void testAdd() {
		Vector3d v1=new Vector3d(2,3,4);
		Vector3d v2=new Vector3d(5,6,7);
		Vector3d vAnswer=new Vector3d(7,9,11);
		assertEquals(vAnswer,v1.add(v2));
	}

}
