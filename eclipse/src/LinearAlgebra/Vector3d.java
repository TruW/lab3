//William Trudel 1939241
package LinearAlgebra;

import java.lang.Math;

public final class Vector3d {
	private final double x;
	private final double y;
	private final double z;
	
	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getZ() {
		return z;
	}
	
	
	public double magnitude() {
		return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z);
	}
	public double dotProduct(Vector3d v) {
		return (this.x*v.getX()+this.y*v.getY()+this.z*v.getZ());
	}
	public Vector3d add(Vector3d v) {
		double xNew=this.x+v.getX();
		double yNew=this.x+v.getY();
		double zNew=this.x+v.getZ();
		Vector3d vNew=new Vector3d(xNew,yNew,zNew);
		return vNew;
	}
	
}
